# PyWhiptail

Some Useful Python snippets

## whiptail.py

a class using subprocesses to create whiptail dialogs
## tee.py

a class that spawns a thread to pass io from a single input to multiple outputs.
Outputs can be io.BaseIO or hashlib members
__main__ implements writing stdin to files and stdout.