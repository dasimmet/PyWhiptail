import os
import unittest
import pep8


class Pep8Test(unittest.TestCase):
    MAXIMUM_ALLOWED_ERRORS = 0

    def test_pep8(self):
        style = pep8.StyleGuide()
        style.options.max_line_length = 100  # Set this to desired maximum line length
        filenames = []
        dirname = os.path.normpath(os.path.join(__file__, '..', '..'))
        # Set this to desired folder location
        for root, dirs, files in os.walk(dirname):
            python_files = [f for f in files if f.endswith('.py')]
            for file in python_files:
                filename = os.path.join(root, file)
                filenames.append(filename)
        check = style.check_files(filenames)
        self.assertEqual(check.total_errors, self.MAXIMUM_ALLOWED_ERRORS,
                         'PEP8 style errors: %d' % check.total_errors)
