#!/usr/bin/env python3
# PYTHON_ARGCOMPLETE_OK

from argparse import ArgumentParser, Namespace


def main(argv=None):
    if argv is None:
        import sys
        argv = sys.argv[1:]
    p = parser()
    args = p.parse_args(argv)
    return args.func(args) or 0


def parser():
    p = ArgumentParser()
    p.set_defaults(func=lambda _: p.print_help())
    sub = p.add_subparsers()
    p_push = sub.add_parser('push')
    p_push.set_defaults(func=_push_cmd)
    p_push.add_argument('remote')
    sync_args(p_push)
    p_init = sub.add_parser('init')
    p_init.set_defaults(func=_init_cmd)
    p_init.add_argument('name')
    p_init.add_argument('url')
    p_receive = sub.add_parser('__receive')
    p_receive.set_defaults(func=_receive_cmd)
    p_receive.add_argument('remote', nargs='?', default='.')
    sync_args(p_receive)
    try:
        import argcomplete
        argcomplete.autocomplete(p)
    except BaseException:
        pass
    return p


def sync_args(p: ArgumentParser):
    p.add_argument('--head-ref', default='__git_sync_head/')
    p.add_argument('--stash-ref', default='__git_sync_stash/')
    p.add_argument('--keep', action='store_true')


def _init_cmd(args: Namespace):

    remote = Remote(args.url)
    git = Git()

    print(remote.remote)
    remote.init()
    git.set_remote(args.name, remote.url)


def _push_cmd(args: Namespace):

    git = Git('.')

    branch_name = git.current_branch()

    refspec_head = '+HEAD:refs/heads/' + args.head_ref + branch_name
    refspec_stash = '+refs/stash:refs/heads/' + args.stash_ref + branch_name

    has_stash = git.create_stash()

    refspec = [refspec_head, refspec_stash] if has_stash else [refspec_head]

    git.push(args.remote, refspec)

    if not args.keep:
        git.clean_stash()
        git.check_call(['fetch', '-p', args.remote])


def _receive_cmd(args: Namespace):
    import os
    git = Git(args.remote)
    if 'GIT_DIR' in os.environ:
        os.environ.pop('GIT_DIR')

    refs = git.show_ref()
    refs = [l.split()[1] for l in refs]

    head_ref = None
    for ref in refs:
        if ref.startswith('refs/heads/' + args.head_ref):
            head_ref = ref

    if not head_ref:
        return

    base_ref = head_ref.replace('refs/heads/' + args.head_ref, '')
    head_short_ref = args.head_ref + base_ref
    stash_short_ref = args.stash_ref + base_ref
    stash_ref = 'refs/heads/' + stash_short_ref

    if not 'refs/heads/' + base_ref in refs:
        git.check_call(['branch', '-C', head_short_ref, base_ref])

    git.checkout(base_ref)
    git.reset(head_ref)
    git.clean('-ffx')

    if stash_ref in refs:
        git.restore_stash_from_ref(base_ref, head_ref, stash_ref)

    if not args.keep:
        git.delete_branch(head_short_ref)
        if stash_ref in refs:
            git.delete_branch(stash_short_ref)


class Git:
    GIT = ['git']
    cwd = '.'

    def __init__(self, cwd='.'):
        self.cwd = cwd

    @staticmethod
    def stashname(name):
        return 'stash^{/' + name + '}'

    def check_call(self, cmd, *args, **kwargs):
        from subprocess import check_call
        return check_call(self.GIT + cmd, *args, cwd=self.cwd, **kwargs)

    def check_output(self, cmd, *args, **kwargs):
        from subprocess import check_output
        return check_output(self.GIT + cmd, *args, cwd=self.cwd, **kwargs)

    def show_ref(self):
        showref_cmd = ['show-ref']
        return self.check_output(showref_cmd).decode().splitlines()

    def push(self, remote: str, refspec: list):
        push_cmd = ['push', remote, '--']
        self.check_call(push_cmd + refspec)

    def checkout(self, ref: str):
        self.check_call(['checkout', '--force', ref])

    def reset(self, ref):
        reset_cmd = ['reset', '--hard', ref]
        self.check_call(reset_cmd)

    def clean(self, flags):
        clean_cmd = ['clean', flags]
        self.check_call(clean_cmd)

    def set_remote(self, name, url):
        subcommand = 'set-url' if name in self.check_output(
            ['remote']).decode().splitlines() else 'add'
        self.check_call(['remote', subcommand, name, url])

    def delete_branch(self, branch: str):
        delete_branch_cmd = ['branch', '-D', branch]
        self.check_call(delete_branch_cmd)

    def restore_stash_from_ref(self, base_ref, head_ref, stash_ref):
        diffspec = head_ref + '..' + stash_ref

        log_cmd = ['log', '--pretty=%H:%s', diffspec]
        log = self.check_output(log_cmd).decode().splitlines()
        branch = self.current_branch()
        for l in log:
            split = l.split(':')
            rev = split[0]
            msg = ':'.join(split[1:])

            if msg.startswith('On ' + base_ref + ':'):
                self.apply_patch(branch, stash_ref)
            if msg.startswith('index on ' + base_ref + ':'):
                self.apply_patch(branch, rev, index=True)
            if msg.startswith('untracked files on ' + base_ref + ':'):
                self.unarchive_rev(rev)

    def apply_patch(self, branch, ref, index=False):
        diffspec = branch + '..' + ref
        diff = self.check_output(['diff', diffspec])

        add_cmd = ['apply', '--allow-empty', '--whitespace=nowarn']
        if index:
            add_cmd.append('--cached')
        import tempfile
        with tempfile.TemporaryFile() as fd:
            fd.write(diff)
            fd.flush()
            fd.seek(0)
            self.check_output(add_cmd + ['-'], stdin=fd)

    def unarchive_rev(self, rev):
        arc = self.check_output(['archive', '--format=tar', '--', rev])
        from io import BytesIO
        import tarfile
        with BytesIO(arc) as arcfile:
            try:
                with tarfile.open(fileobj=arcfile) as tf:
                    tf.extractall(self.cwd)
            except tarfile.TarError:
                print('No archived files to restore')

    def clean_stash(self, name='__git_sync'):
        from subprocess import check_call
        list_cmd = ['stash', 'list']
        drop_cmd = ['stash', 'drop']
        stash_list = self.check_output(list_cmd).decode().splitlines()
        for s in stash_list:
            if s.startswith('stash@') and s.endswith(': ' + name):
                self.check_call(drop_cmd + s.split(':')[:1])

    def create_stash(self, name='__git_sync'):
        from subprocess import check_call
        create_cmd = ['stash', 'push', '-u', '-m', name]
        apply_cmd = ['stash', 'apply', '--index', Git.stashname(name)]
        self.clean_stash(name=name)
        try:
            self.check_call(create_cmd)
            self.check_call(apply_cmd)
            return True
        except BaseException:
            return False

    def current_branch(self):
        return self.check_output(['branch', '--show-current']).decode().strip()


class Remote:
    SSH_CMD = ['ssh']

    def __init__(self, remote: str):
        from urllib.parse import urlparse
        colon_index = remote.find(':')
        if colon_index > 0 and remote.find('://') < 0:

            remote = remote[:colon_index] + '/~/' + remote[colon_index + 1:]
            remote = 'ssh://' + remote
        remote = urlparse(remote)
        if remote.scheme == '':
            from posixpath import abspath
            remote = remote._replace(scheme='file', path=abspath(remote.path))
        self.remote = remote

    @property
    def url(self):
        return self.remote.geturl()

    def init(self):
        from subprocess import check_call
        from posixpath import join
        rsh = self.__rsh
        rsh_path = self.__rsh_path
        mkdir_cmd = rsh + ['mkdir', '-p', '--', rsh_path]
        init_cmd = rsh + ['git', 'init', '--', rsh_path]

        hook_path = join(rsh_path, '.git', 'hooks', 'post-receive')
        hook_cmd = rsh + ['sh', '-c', 'cat > ' + hook_path]
        chmod_cmd = rsh + ['chmod', '+x', hook_path]

        print("Initializing Remote Repository")
        print(mkdir_cmd)
        check_call(mkdir_cmd)
        print(init_cmd)
        check_call(init_cmd)
        print("Setting up remote post-receive hook")
        print(hook_cmd)
        with open(__file__) as fd:
            file = fd.read()
        file = set_default_args(['__receive', '..'], file)
        import tempfile
        with tempfile.TemporaryFile() as fd:
            fd.write(file.encode())
            fd.flush()
            fd.seek(0)
            check_call(hook_cmd, stdin=fd)
        print(chmod_cmd)
        check_call(chmod_cmd)

    @property
    def __rsh_path(self):
        if self.remote.scheme == 'ssh' and self.remote.path.startswith('/~'):
            return self.remote.path.lstrip('/')
        return self.remote.path

    @property
    def __rsh(self):
        remote = self.remote
        if remote.scheme == 'ssh':
            rsh = self.SSH_CMD.copy()
            if remote.port not in [None, '']:
                rsh.extend(['-p', remote.port])
            rsh.extend(['--', remote.netloc])
            return rsh
        return []


def set_default_args(args: list, file: str):
    ipos = file.find(DEFAULT_ARGS_MARKER) + len(DEFAULT_ARGS_MARKER) + 2
    opos = file.find(DEFAULT_ARGS_MARKER_END) + \
        len(DEFAULT_ARGS_MARKER_END) + 1
    return (file[:ipos] +
            "DEFAULT_ARGS = " + str(args) +
            file[opos:])


DEFAULT_ARGS_MARKER = "__REPLACEME_DEFAULT_ARGS_MARKER"
DEFAULT_ARGS = None
DEFAULT_ARGS_MARKER_END = "__REPLACEME_DEFAULT_ARGS_MARKER_END"

if __name__ == '__main__':
    import sys
    sys.exit(main(DEFAULT_ARGS))
