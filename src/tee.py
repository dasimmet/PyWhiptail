#!/usr/bin/env python3
# PYTHON_ARGCOMPLETE_OK

import io


class Tee(io.IOBase):
    """
        a threading based IO class to write a single
        input stream to multiple outputs
        it creates an os.pipe() so a subprocess can be
        used as input. The class is supposed to
        be only used by the 'with' syntax:
        with Tee(outputs=[...]) as tee:
            tee.write(x)
    """
    BUFSIZE: int = io.DEFAULT_BUFFER_SIZE

    def __init__(self, outputs: list = [], BUFSIZE: int = None) -> None:
        if BUFSIZE is not None:
            self.BUFSIZE = BUFSIZE
        self.outputs = outputs

    def __enter__(self):
        import os
        import threading
        self.pipe = os.pipe()
        self.thread = threading.Thread(target=self._task)
        self.thread.start()
        return self

    def __exit__(self, *args):
        self.close()

    def _task(self):
        import os
        writers = []
        for o in self.outputs:
            # if o is an io class
            if isinstance(o, io.IOBase):
                writers.append(o.write)
            # if o is from hashlib
            elif hasattr(o, 'update') and hasattr(o, 'digest'):
                writers.append(o.update)

        while True:
            buffer = os.read(self.pipe[0], self.BUFSIZE)
            if buffer == b'':
                break
            for writer in writers:
                writer(buffer)

    def write(self, input):
        import os
        os.write(self.pipe[1], input)

    def fileno(self):
        return self.pipe[1]

    def close(self):
        import os
        os.close(self.pipe[1])
        self.thread.join()
        os.close(self.pipe[0])


def _parser():
    from argparse import ArgumentParser, RawDescriptionHelpFormatter
    import hashlib
    p = ArgumentParser(formatter_class=RawDescriptionHelpFormatter, epilog="""
Example:
> tar c . | xz -c | ./tee.py -d sha1 -d sha256 -d md5 -s 'sha1sum -' /tmp/out.tar.xz

sha1: 91d1882b6e458326366e53a71820864b0f52bbb7
sha256: 936c20ff163ae0712447157d1818bc1bee0ac72911ed1c91c843585329bc6857
md5: 71cae3c81750ea333e6dd8fb56c8cace
91d1882b6e458326366e53a71820864b0f52bbb7  -

""")

    def UINT(arg: str, parser=p):
        if int(arg) < 0:
            parser.error('UINT should be above 0: {}'.format(arg))
        else:
            return int(arg)

    import os

    def FILE(arg: str, parser=p, mode=os.R_OK, mode_hint='readable'):
        import os
        if arg == '-' or (not os.path.isdir(arg) and os.access(arg, mode)):
            return arg
        parser.error('Not a {} file: {}'.format(mode_hint, arg))

    def WRITEFILE(arg: str):
        return FILE(arg, mode=os.W_OK, mode_hint='writeable')

    p.add_argument(
        '-a',
        '--append',
        help="append to output files instead of replacing",
        action='store_true')
    p.add_argument(
        '-b',
        '--bufsize',
        help="overwrite default read/write block size",
        default=Tee.BUFSIZE,
        type=UINT)
    p.add_argument('-s', '--shell', help="shell subprocess to pass stdin",
                   action='append', default=[])
    p.add_argument(
        '-i',
        '--input',
        help="input file or stdin by default",
        default='-',
        type=FILE)
    p.add_argument(
        '-d',
        '--digest',
        help="pass input to hashlib algorithm",
        action='append',
        default=[],
        choices=hashlib.algorithms_guaranteed)
    p.add_argument('output', help='output files', nargs='*', type=WRITEFILE)

    try:
        import argcomplete
        argcomplete.autocomplete(p)
    except BaseException:
        pass

    return p


def main(argv: list = None):
    import sys
    import hashlib
    import subprocess

    if argv is None:
        argv = sys.argv[1:]
    p = _parser()
    args = p.parse_args(argv)

    if args.input == '-':
        reader = sys.stdin.buffer
    else:
        reader = open(args.input, 'rb')

    files = []
    mode = 'ab' if args.append else 'wb'
    for i in args.output:
        if i == '-':
            files.append((sys.stdout.buffer, sys.stdout.buffer.close))
        else:
            fd = open(i, mode)
            files.append((fd, fd.close))

    for i in args.digest:
        fd = hashlib.new(i)
        files.append((fd, lambda fd=fd: print(
            '{}: {}'.format(fd.name, fd.hexdigest()), file=sys.stderr)))

    for i in args.shell:
        fd = subprocess.Popen(i, shell=True, stdin=subprocess.PIPE)
        files.append((fd.stdin, lambda fd=fd: fd.stdin.close() and fd.wait()))

    with Tee(outputs=[f for f, _ in files], BUFSIZE=args.bufsize) as tee:
        while True:
            buf = reader.read(tee.BUFSIZE)
            if buf == b'':
                break
            tee.write(buf)

    for _, cb in files:
        cb()


if __name__ == '__main__':
    main()
