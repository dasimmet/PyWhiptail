#!/usr/bin/env python3

from typing import NamedTuple


class WTOpts(NamedTuple):
    """Options tuple with defaults for Whiptail class
    """
    exe = ["whiptail"]
    height: int = 0
    width: int = 0
    menuheight: int = 0
    title: str = None
    backtitle: str = None
    debug: bool = False
    defaultno: bool = False
    yes_button: str = None
    fullbuttons: bool = False


class WtResult(NamedTuple):
    out: str = None
    code: int = 0


class Whiptail:
    """Terminal Gui Class for Whiptail using subprocesses
    """
    _opt: WTOpts = None
    _opt_override: WTOpts = None

    def __init__(self, opt: WTOpts = WTOpts()):
        self._opt = opt

    @property
    def opt(self):
        if self._opt_override is not None:
            return self._opt_override
        return self._opt

    @opt.setter
    def opt(self, value):
        self._opt = value

    def with_opt(self, **kwargs: WTOpts._field_defaults):
        self._opt_override = self.opt._replace(**kwargs)
        return self

    def _format_proc(self, ARGS: list, KWARGS: list):

        if self.opt.backtitle is not None:
            KWARGS = ['--backtitle', self.opt.backtitle] + KWARGS

        if self.opt.title is not None:
            KWARGS = ["--title", self.opt.title] + KWARGS

        if self.opt.yes_button is not None:
            KWARGS = ["--yes-button", self.opt.yes_button] + KWARGS

        if self.opt.defaultno:
            KWARGS = ["--defaultno"] + KWARGS

        if self.opt.fullbuttons:
            KWARGS = ["--fullbuttons"] + KWARGS

        return self.opt.exe + KWARGS + ['--'] + ARGS

    def _proc(self, ARGS: list, KWARGS: list) -> WtResult:
        import subprocess
        CMD = self._format_proc(ARGS, KWARGS)

        if self.opt.debug:
            print(CMD)

        self._opt_override = None
        p = subprocess.Popen(CMD, stderr=subprocess.PIPE)

        out, err = p.communicate()
        res = err.decode() if err is not None else err

        return WtResult(code=p.returncode, out=res)

    def numbered_menu(self, text: str, optionlist: list,
                      format=None, offset=1) -> int:
        opts = []
        if not format:
            def format(x): return str(x) + ')'
        i = offset
        for it in optionlist:
            opts.append((format(i), it))
            i += 1
        res = self.menu(text, opts)
        if res.code == 0:
            i = 0
            for it in optionlist:
                i += 1
                if format(i) == res.out:
                    return i - offset
        else:
            return None

    def menu(self, text: str, options: list):
        KWARGS = ["--menu", text]
        ARGS = [str(self.opt.height), str(
            self.opt.width), str(self.opt.menuheight)]

        for k, v in options:
            ARGS.extend((str(k), str(v)))

        return self._proc(ARGS, KWARGS)

    def _msgbox(self, msg: str, type: str):
        KWARGS = [type, msg]
        ARGS = [str(self.opt.height), str(self.opt.width)]

        return self._proc(ARGS, KWARGS)

    def radiolist(self, msg: str, options: list):
        return self._proc_checklist(msg, options, type='--radiolist')

    def checklist(self, msg: str, options: list):
        for key, _, _ in options:
            if key.find('"') >= 0:
                raise ChecklistKeyException(
                    'Checklist keys containing " are not parseable, sorry: {}'.format(key))
        res = self._proc_checklist(msg, options)
        result_list = []
        if res.code != 0:
            return [res]
        else:
            split = res.out.split('"')
            for it in split:
                if it not in ['', ' ']:
                    result_list.append(it)

        return [res._replace(out=it) for it in set(result_list)]

    def _proc_checklist(self, msg: str, options: list, type='--checklist'):
        KWARGS = [type, msg]
        ARGS = [str(self.opt.height), str(
            self.opt.width), str(self.opt.menuheight)]

        for tag, item, status in options:
            ARGS.extend((str(tag), str(item), str('yes' if status else 'no')))

        return self._proc(ARGS, KWARGS)

    def _inputbox(self, msg: str, type: str, init: str = None):
        KWARGS = [type, msg]
        ARGS = [str(self.opt.height), str(self.opt.width)]
        if init is not None:
            ARGS.append(init)

        return self._proc(ARGS, KWARGS)

    def inputbox(self, msg: str, init: str = None):
        return self._inputbox(msg, "--inputbox", init)

    def passwordbox(self, msg: str, init: str = None):
        return self._inputbox(msg, "--passwordbox", init)

    def msgbox(self, msg: str):
        return self._msgbox(msg, "--msgbox")

    def textbox(self, msg: str):
        return self._msgbox(msg, "--textbox")

    def infobox(self, msg):
        return self._msgbox(msg, "--infobox")

    def yesno(self, msg):
        return self._msgbox(msg, "--yesno")

    def gauge(self, msg: str, percent: float = 0):
        KWARGS = ['--gauge', msg]
        ARGS = [str(self.opt.height), str(self.opt.width), str(percent)]

        CMD = self._format_proc(ARGS, KWARGS)
        self._opt_override = None
        return Gauge(self, CMD)


class ChecklistKeyException(Exception):
    pass


class PercentageException(Exception):
    pass


class Gauge:
    """wrapper class to handle gauge with "with" syntax:
        with wt.gauge(...) as gauge:
            for i in range(100):
                gauge.update(i)
    """

    def __init__(self, wt: Whiptail, CMD: list):
        self.wt = wt
        self.CMD = CMD

    def update(self, percent: float = 0):
        if percent < 0 or percent > 100:
            raise PercentageException(
                "Percentages should be within 0-100: {}".format(percent))
        self.proc.stdin.write((str(percent) + '\n').encode())
        self.proc.stdin.flush()

    def __enter__(self):
        import subprocess
        self.proc = subprocess.Popen(
            self.CMD, stdin=subprocess.PIPE, stderr=subprocess.PIPE)
        return self

    def __exit__(self, *args):
        if not self.proc.stdin.closed:
            self.proc.stdin.close()
        self.proc.wait()


class WhiptailMenu:
    result = None

    def __init__(self, wt: Whiptail, data: dict = {}) -> None:
        self.wt = wt
        self.data = data

    def start(self):
        wt = self.wt
        if 'opts' in self.data:
            wt = wt.with_opt(**self.data['opts'])

        if 'cmd' in self.data:
            import subprocess
            out = subprocess.check_output(self.data['cmd'])
            wt.with_opt(title=self.data['text']).msgbox(out.strip())

        if 'f_enter' in self.data:
            self.data['f_enter'](self)

        if 'type' in self.data and self.data['type'] == 'menu':
            res = True
            while res is not None:
                res = wt.numbered_menu(
                    self.data['text'], [
                        o['text'] for o in self.data['entries']])
                if res is not None:
                    self.result = self.data['entries'][res]
                    submenu = self.__class__(
                        self.wt, self.result)
                    submenu.start()

                if 'f_return' in self.data:
                    self.data['f_return'](self)


def _print_env(wtm: WhiptailMenu):
    import os
    boxes = []
    i = True
    for k, v in os.environ.items():
        boxes.append((k, v, i))
        i = False
    res = wtm.wt.checklist("Environment", boxes)
    for r in res:
        if r.code != 0:
            break
        wtm.data['context'].append(
            "{}={}".format(r.out, os.environ.get(r.out)))
    print("RES: {}".format(res))
    k = wtm.wt.msgbox("RES: {}".format(wtm.data['context']))


WHIPTAILMENU_EXAMPLE = {
    'type': 'menu',
    'text': 'Whiptailmenu Example',
    'entries': [
        {
            "type": "menu",
            "text": "submenu",
            "entries": [
                {
                    "text": "Print Hostname",
                    "cmd": ["hostname"],
                },
                {
                    "text": "ping google",
                    "cmd": ["ping", '-c5', '8.8.8.8'],
                },
            ]
        },
        {
            "text": "Print Hostname",
            "cmd": ["hostname"],
        },
        {
            "text": "ip addr",
            "cmd": ["ip", "addr"],
        },
        {
            "text": "Print Context",
            "func": lambda wtm: wtm.wt.msgbox(str(wtm.__dict__)),
        },
        {
            "text": "Show and pick environment variables",
            "f_enter": _print_env,
            'context': []
        },
    ],
}


def main():
    wt = Whiptail(opt=WTOpts(
        title="WOLOLO",
        debug=True,
        backtitle="Whiptail Test",
    ))

    with wt.with_opt(yes_button="OKEDOKE", width=100).gauge("wololo") as gauge:
        from time import sleep
        for i in range(-100, 100):
            gauge.update(100 - abs(i))
            sleep(0.01)

    WhiptailMenu(wt.with_opt(fullbuttons=True), WHIPTAILMENU_EXAMPLE).start()


if __name__ == '__main__':
    main()
