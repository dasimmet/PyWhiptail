#!/usr/bin/env python3

from serial import Serial
import io
import subprocess
from urllib.request import urlopen, Request
from urllib.parse import urlencode, urlparse
import json
API = urlparse('https://barcodedeutschland.de/wp-admin/admin-ajax.php')
KEY = '5898dd07-5749-4898-b655-d40bcb83c98b'
HEADERS = {
    'Content-Type': 'application/json',
    # 'Apikey': KEY,
}

with Serial('/dev/ttyUSB0') as ser:
    while True:
        hello = ser.read_until(b'\r')
        # hello = ser.read()
        rstr = hello.decode()

        data = json.dumps({
            'type': 'barcode',
            'search': rstr.strip(),
            'action': 'moso_gebir',
        }).encode()
        print(data)

        with urlopen(Request(API.geturl(), data=data, headers=HEADERS)) as res:
            print(res)
            print(res.read())

        # subprocess.check_call(['xdg-open', 'https://www.ean-search.org/perl/ean-search.pl?q={}'.format(rstr)])
