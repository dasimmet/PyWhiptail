#!/usr/bin/env python3

import gtts
import html2text
from urllib.request import urlopen, Request
import sys

h = html2text.HTML2Text()
h.ignore_links = True
txt = urlopen(Request(
    sys.argv[1],
    headers={
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'}
)).read().decode()
gtts.gTTS(h.handle(txt), lang="en", slow=False).save(sys.argv[2])
