#!/usr/bin/env python3

import yaml
import jinja2

class YamlTemplate(jinja2.Environment):
    __INTERNAL_FLAG = "__yamltemplate_internal:"
    RECURSE_DEPTH = 10
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.filters['yaml'] = self.__to_yaml
        self.filters['yaml_dump'] = yaml.safe_dump
        self.filters['yaml_load'] = yaml.safe_load

    def process(self, obj, top=None, depth=0):
        if top is None:
            top = obj

        if isinstance(obj, str):
            tpl = self.from_string(obj)
            newval = tpl.render(top)
            if newval != obj:
                if newval.startswith(self.__INTERNAL_FLAG):
                    newval = yaml.safe_load(newval[len(self.__INTERNAL_FLAG):])
                if depth > self.RECURSE_DEPTH:
                    raise RecursionError(depth)
                return self.process(newval, top=top, depth=depth + 1)

        if isinstance(obj, dict):
            for k, v in obj.items():
                obj[k] = self.process(obj[k], top=top, depth=depth)

        if isinstance(obj, list):
            for i in range(len(obj)):
                obj[i] = self.process(obj[i], top=top, depth=depth)

        return obj
    
    def __to_yaml(self, obj, *args, **kwargs):
        import types

        if isinstance(obj, types.GeneratorType):
            obj = list(obj)

        res = yaml.safe_dump(obj, *args, **kwargs)
        return self.__INTERNAL_FLAG + res


if __name__ == '__main__':
    import sys

    with open(sys.argv[1]) as fd:
        obj = yaml.safe_load(fd)
    print("Yamlloaded...")

    print(yaml.safe_dump(obj))
    print("Processing...")
    
    env = YamlTemplate()
    obj = env.process(obj)

    print(yaml.safe_dump(obj))
